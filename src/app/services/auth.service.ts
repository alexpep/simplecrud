import { Injectable } from '@angular/core';
import { User } from '../models/user.model';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private _currentUser: User;
  get currentUser(): User{
    return this._currentUser;
  }
  constructor() {
    this._currentUser = JSON.parse(localStorage.getItem("CU"));
   }

  login(email: string, password: string): User{
    if (email === 'a@a' && password === 'a'){
        this._currentUser=new User(email);
        localStorage.setItem("CU",JSON.stringify(this._currentUser));

      return this._currentUser;
    }
    else{
      alert('Invalid credentials!')
    }

   /* 5 =="5" // true
      5 === "5" // false
   */
  }


  logout(){
    this._currentUser = null
    localStorage.setItem("CU",null);
  }
}
